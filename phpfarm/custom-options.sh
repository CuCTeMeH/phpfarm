# we need the correct path, is there a better way to find it?
if [ -d "/lib/i386-linux-gnu" ]; then
    LIBPATH="/lib/i386-linux-gnu/"
else
    LIBPATH="/usr/lib/x86_64-linux-gnu/"
fi

configoptions="\
 --with-libdir=lib/x86_64-linux-gnu
 --with-regex=php \
 --enable-sysvsem \
 --enable-sysvshm \
 --enable-sysvmsg \
 --enable-track-vars \
 --enable-trans-sid \
 --with-bz2 \
 --enable-ctype \
 --without-gdbm \
 --with-iconv \
 --enable-filepro \
 --enable-fastcgi \
 --enable-cgi \
 --enable-pcntl \
 --enable-shmop \
 --with-kerberos=/usr/lib \
 --with-openssl \
 --enable-dbx \
 --with-system-tzdata \
 --with-mysql=mysqlnd \
 --with-fpm-user=www-data \
 --with-fpm-group=www-data \
 --enable-fpm \
 --with-mysqli=/usr/bin/mysql_config \
 --enable-pdo \
 --with-pdo-mysql=/usr/bin/mysql_config \
 --with-curl \
 --enable-bcmath \
 --enable-calendar \
 --enable-exif \
 --enable-ftp \
 --with-gd \
 --with-jpeg-dir \
 --with-png-dir \
 --with-zlib-dir=/usr/lib \
 --with-gettext=/usr/lib \
 --enable-mbstring \
 --with-mcrypt=/usr/lib \
 --with-mhash \
 --with-mime-magic \
 --enable-soap \
 --enable-sockets \
 --with-tidy \
 --enable-wddx \
 --with-xsl=/usr/lib \
 --with-zip \
 --with-zlib \
 --enable-zip \
 --with-imagick \
"

echo $configoptions
