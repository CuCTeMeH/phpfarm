#!/bin/bash
USER=root

PHP_52_BIND=127.0.0.1:9052
PHP_53_BIND=127.0.0.1:9053
PHP_54_BIND=127.0.0.1:9054
PHP_55_BIND=127.0.0.1:9055
PHP_56_BIND=127.0.0.1:9056

PHP_52_CGI=/phpfarm/inst/php-5.2.17/bin/php-cgi
PHP_53_CGI=/phpfarm/inst/php-5.3.29/bin/php-cgi
PHP_54_CGI=/phpfarm/inst/php-5.4.43/bin/php-cgi
PHP_55_CGI=/phpfarm/inst/php-5.5.27/bin/php-cgi
PHP_56_CGI=/phpfarm/inst/php-5.6.11/bin/php-cgi

PHP_FCGI_CHILDREN=15
PHP_FCGI_MAX_REQUESTS=1000

PHP_CGI_ARGS="- USER=$USER PATH=/usr/bin PHP_FCGI_CHILDREN=$PHP_FCGI_CHILDREN PHP_FCGI_MAX_REQUESTS=$PHP_FCGI_MAX_REQUESTS"
PHP_52_CGI_ARGS="${PHP_CGI_ARGS} ${PHP_52_CGI} -b ${PHP_52_BIND}"
PHP_53_CGI_ARGS="${PHP_CGI_ARGS} ${PHP_53_CGI} -b ${PHP_53_BIND}"
PHP_54_CGI_ARGS="${PHP_CGI_ARGS} ${PHP_54_CGI} -b ${PHP_54_BIND}"
PHP_55_CGI_ARGS="${PHP_CGI_ARGS} ${PHP_55_CGI} -b ${PHP_55_BIND}"
PHP_56_CGI_ARGS="${PHP_CGI_ARGS} ${PHP_56_CGI} -b ${PHP_56_BIND}"


# First kill nginx
killall -q -w -u ${USER} nginx

# Then kill fastcgi daemons
killall -q -w -u ${USER} ${PHP_53_CGI}
killall -q -w -u ${USER} ${PHP_52_CGI}

# Launch fastcgi daemons & nginx
/usr/bin/env -- $PHP_52_CGI_ARGS &
/usr/bin/env -- $PHP_53_CGI_ARGS &
/usr/bin/env -- $PHP_54_CGI_ARGS &
/usr/bin/env -- $PHP_55_CGI_ARGS &
/usr/bin/env -- $PHP_56_CGI_ARGS &
/usr/sbin/nginx