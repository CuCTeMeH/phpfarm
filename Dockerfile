#
# PHP Farm Docker image
#

# we use Ubuntu Trusty as the host OS
FROM ubuntu:trusty

MAINTAINER Plamen Kutinchev, kutinchev87@gmail.com

RUN apt-get update && apt-get install -y python
RUN apt-get update && apt-get -y install software-properties-common
RUN apt-add-repository ppa:ondrej/mysql-5.6
RUN apt-add-repository multiverse
RUN apt-add-repository ppa:chris-lea/redis-server
RUN add-apt-repository ppa:nginx/stable

# add some build tools
RUN apt-get update && \
    apt-get install -y --force-yes \
    nginx \
    git \
    libsasl2-dev \
    libtidy-dev \
    libmysqlclient-dev \
    imagemagick \
    libxslt1-dev \
    libxslt1.1 \
    redis-server \
    build-essential \
    wget \
    openjdk-7-jdk \
    solr-tomcat \
    memcached \
    libxml2-dev \
    libssl-dev \
    libsslcommon2-dev \
    libcurl4-openssl-dev \
    pkg-config \
    curl \
    libapache2-mod-fcgid \
    libbz2-dev \
    libjpeg-dev \
    libpng-dev \
    libfreetype6-dev \
    libxpm-dev \
    libmcrypt-dev \
    libt1-dev \
    libmemcached-dev \
    libltdl-dev \
    vim-nox \
    autoconf \
    nano \
    curl \
    libmhash-dev


RUN mkdir /usr/java
RUN ln -s /usr/lib/jvm/java-7-openjdk-amd64 /usr/java/default
RUN ln -s /usr/lib/libjpeg.so /usr/lib/libjpeg.so.62.0.0

# install and run the phpfarm script
#RUN git clone git://git.code.sf.net/p/phpfarm/code phpfarm

# add customized configuration
COPY phpfarm /phpfarm/src/

# compile, then delete sources (saves space)
RUN cd /phpfarm/src && \
    ./compile.sh 5.2.17 && \
    ./compile.sh 5.3.29 && \
    ./compile.sh 5.4.43 && \
    ./compile.sh 5.5.27 && \
    ./compile.sh 5.6.11 && \
    #rm -rf /phpfarm/src && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN chmod 777 /phpfarm/inst/php-5.5.27/pyrus.phar

#install memcached extension
RUN cd /phpfarm && /phpfarm/inst/bin/php-5.5.27 /phpfarm/inst/php-5.5.27/pyrus.phar download pecl/memcached
RUN cd /phpfarm/ && tar xvf memcached-2.2.0.tgz
RUN cd /phpfarm/memcached-2.2.0/ && /phpfarm/inst/bin/phpize-5.5.27 && ./configure --disable-memcached-sasl --with-php-config=/phpfarm/inst/bin/php-config-5.5.27
RUN cd /phpfarm/memcached-2.2.0/ && make && make install
RUN echo 'extension=memcached.so' >> /phpfarm/inst/php-5.5.27/lib/php.ini

#install redis extensions
RUN cd /phpfarm && /phpfarm/inst/bin/php-5.6.11 /phpfarm/inst/php-5.5.27/pyrus.phar download pecl/redis
RUN cd /phpfarm/ && tar xvf redis-2.2.7.tgz
RUN cd /phpfarm/redis-2.2.7/ && /phpfarm/inst/bin/phpize-5.6.11 && ./configure --with-php-config=/phpfarm/inst/bin/php-config-5.6.11
RUN cd /phpfarm/redis-2.2.7/ && make && make install
RUN echo 'extension=redis.so' >> /phpfarm/inst/php-5.6.11/lib/php.ini

#install solr extensions
RUN cd /phpfarm && /phpfarm/inst/bin/php-5.6.11 /phpfarm/inst/php-5.5.27/pyrus.phar download pecl/solr
RUN cd /phpfarm/ && tar xvf solr-2.1.0.tgz
RUN cd /phpfarm/solr-2.1.0/ && /phpfarm/inst/bin/phpize-5.6.11 && ./configure --with-php-config=/phpfarm/inst/bin/php-config-5.6.11
RUN cd /phpfarm/solr-2.1.0/ && make && make install
RUN echo 'extension=solr.so' >> /phpfarm/inst/php-5.6.11/lib/php.ini

#RUN update-rc.d memcached enable
#RUN echo "memcached -uroot -d" >> /etc/rc.local
#RUN ln -s "memcached -uroot -d" ~/.config/autostart

# reconfigure Apache
RUN rm -rf /var/www/*

COPY var-www /var/www/html/
COPY apache  /etc/apache2/

RUN a2ensite php-5.2.conf php-5.3.conf php-5.4.conf php-5.5.conf php-5.6.conf
#RUN a2ensite php-5.5.conf

RUN a2enmod rewrite actions fastcgi alias ssl

# set path
ENV PATH /phpfarm/inst/bin/:/usr/sbin:/usr/bin:/sbin:/bin

# expose the ports
EXPOSE 8052 8053 8054 8055 8056 8080

# run it
COPY run.sh /run.sh
ENTRYPOINT ["/bin/bash"]
CMD ["/run.sh"]
